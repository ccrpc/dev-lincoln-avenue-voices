# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [1.1.1](https://gitlab.com/ccrpc/lincoln-avenue-voices/compare/v1.1.0...v1.1.1) (2023-09-13)


### Bug Fixes

* Fixing driver.js css ([c55debe](https://gitlab.com/ccrpc/lincoln-avenue-voices/commit/c55debe2674d3186e04ad720cdc705361b65e855))

## [1.1.0](https://gitlab.com/ccrpc/lincoln-avenue-voices/compare/v1.0.0...v1.1.0) (2023-09-06)


### Features

* Adding glyphs ([df4eed1](https://gitlab.com/ccrpc/lincoln-avenue-voices/commit/df4eed1e25627665b97af31675c3372a2093c95d))
* DIsable Rotate ([f5a13be](https://gitlab.com/ccrpc/lincoln-avenue-voices/commit/f5a13be5041beb11ef9e112badd534bf7dbebd9b))
* Limit map ([9a40ba0](https://gitlab.com/ccrpc/lincoln-avenue-voices/commit/9a40ba0b6ee5408d73eca22c549611f8c825f60d))
* **skaffold:** no longer need to include /lincoln-avenue-voices ([944dd59](https://gitlab.com/ccrpc/lincoln-avenue-voices/commit/944dd59ff3402d8226491666f6fe44e5f02fb9f9))
* Updated text ([0110d09](https://gitlab.com/ccrpc/lincoln-avenue-voices/commit/0110d09336ab9e7efb6e56c4e80b1be9aa679490))


### Bug Fixes

* Adding features back in ([037e745](https://gitlab.com/ccrpc/lincoln-avenue-voices/commit/037e745efcdd0eb0909007cf9c8aada211d4e4ec))
* Fixed css import ([5d879e2](https://gitlab.com/ccrpc/lincoln-avenue-voices/commit/5d879e2724b416107dcc9c1068d6ab51e8713881))
* using translateText instead of translate ([c992685](https://gitlab.com/ccrpc/lincoln-avenue-voices/commit/c992685142aad461de815c7db7e026b879e4e5a1))

## 1.0.0 (2023-04-27)


### Features

* Added like functionality ([cd79b7f](https://gitlab.com/ccrpc/transportation-voices-2050/commit/cd79b7f990be73f6931005facaad532b1cb32e52))
* Allow like based on allowInput ([19a8eb6](https://gitlab.com/ccrpc/transportation-voices-2050/commit/19a8eb605dd520eb412f2134c05c8044ab5717d6))
* Attempting label click ([d9b1ad2](https://gitlab.com/ccrpc/transportation-voices-2050/commit/d9b1ad28c8f7a7ee0c0b1ef2de5f05bbb0f39611))
* Attempting scroll select ([757372f](https://gitlab.com/ccrpc/transportation-voices-2050/commit/757372f826d4d8989e9cdcf66754691e1086f751))
* Attempting to find selected item ([787a399](https://gitlab.com/ccrpc/transportation-voices-2050/commit/787a3995baf081c58a580ee831a601d45de413ee))
* Dissalow selected like ([95cbd59](https://gitlab.com/ccrpc/transportation-voices-2050/commit/95cbd59f29690c7f1101f3793b957b4581fdcf10))
* Likes change feature _like count ([37c6b5f](https://gitlab.com/ccrpc/transportation-voices-2050/commit/37c6b5fb2d7ca4c492d6e2ce4af49382a0baf03b))
* Limiting map ([18ca9e2](https://gitlab.com/ccrpc/transportation-voices-2050/commit/18ca9e2c015e0f5995e7557f8a46dd98ca7a16c2))
* Linking to latest survey ([20931bd](https://gitlab.com/ccrpc/transportation-voices-2050/commit/20931bd145bba0f567c50751dee872eaf720b26e))
* Linking up test form to site ([e0ffcd5](https://gitlab.com/ccrpc/transportation-voices-2050/commit/e0ffcd5bf824f3ca1a8ce2834f67e9320b532fac))
* Open survey in new tab ([2f2833e](https://gitlab.com/ccrpc/transportation-voices-2050/commit/2f2833e19e9c987d8651f8c0426938f4aed50316))
* Selected comment ([76abe78](https://gitlab.com/ccrpc/transportation-voices-2050/commit/76abe78f36a4b146e834b8c84289e8567506aeb0))
* Selecting comment opens drawer ([51beab9](https://gitlab.com/ccrpc/transportation-voices-2050/commit/51beab9e6b89c739177ed3d4d2ccbcd79213905d))
* Spanish translation ([f32999d](https://gitlab.com/ccrpc/transportation-voices-2050/commit/f32999d6c665865d1f8fbbd84666baad57b04d5b))
* Step count ([5ead3a3](https://gitlab.com/ccrpc/transportation-voices-2050/commit/5ead3a3293faddd903e473bad7e4b6eb5873c5f7))
* Style selected comment ([f4e4b42](https://gitlab.com/ccrpc/transportation-voices-2050/commit/f4e4b42fa0df51727963ab4ba3debc0e13e4cd33))
* Text for buttons test ([b46ac11](https://gitlab.com/ccrpc/transportation-voices-2050/commit/b46ac11b2a3ff088f1c633b184601a4fbcc340b1))
* Working on enableing input for 2050 ([9117de7](https://gitlab.com/ccrpc/transportation-voices-2050/commit/9117de7f3b3e8765c1d69565add482261c29da8b))


### Bug Fixes

* Fix errant comman in es.ts ([a40d806](https://gitlab.com/ccrpc/transportation-voices-2050/commit/a40d8064c3e6c6a0e7395d91f7530f19add22fd9))
* Fix show like if not selected id ([ddf30dc](https://gitlab.com/ccrpc/transportation-voices-2050/commit/ddf30dcbfa84c62521388b1007131f9c97cafe15))
* For the gl-drawer-toggle click button inside it ([bc0ed28](https://gitlab.com/ccrpc/transportation-voices-2050/commit/bc0ed288d1aefc05a5beb62a0d06a5afa4103598))
* Listener was being overwritten by object ([441940f](https://gitlab.com/ccrpc/transportation-voices-2050/commit/441940f7d3f7bb9c685f44e8baee9e5f29f1d594))
* **scroll:** Fixing infinite loop while debugging ([a390be8](https://gitlab.com/ccrpc/transportation-voices-2050/commit/a390be82099a37ed4d070157f9438400a9f325b8))
* **scroll:** scrolling on child ([6969cd1](https://gitlab.com/ccrpc/transportation-voices-2050/commit/6969cd1897ab3f3ffe8d9f1ce475b574a2172237))
* update features URL ([2697279](https://gitlab.com/ccrpc/transportation-voices-2050/commit/2697279a21b6b0406b6c03aae30de8906fe5c3e4))
* update geocoder URL ([cf17f56](https://gitlab.com/ccrpc/transportation-voices-2050/commit/cf17f5660766988a06809dcb0ad92c5217d7be11))
* Using up-to-date stencil includes ([380f286](https://gitlab.com/ccrpc/transportation-voices-2050/commit/380f2866ea41bf3b9123260f3ba633b83254e949))
