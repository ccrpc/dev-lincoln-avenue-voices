import {
  h,
  Component,
  Element,
  Listen,
  Prop,
  State,
  Host,
} from "@stencil/core";
import { findOrCreateOnReady } from "../utils";
import { Driver, driver as createDriver } from "driver.js";
import { _t } from "../i18n/i18n";
import { Feature, Point } from "geojson";

@Component({
  styleUrls: ["app.css", "../../../node_modules/driver.js/dist/driver.css"],
  tag: "lrtp-app",
})
export class App {
  private surveyCtrl?: HTMLLrtpSurveyControllerElement;
  private map?: HTMLGlMapElement;
  private drawerToggle?: HTMLGlDrawerToggleElement;
  private surveyButton?: HTMLIonButtonElement;

  @Element() el: HTMLLrtpAppElement;

  /**
   * Whether users who view the map are allowed to add new comments or use the survey.
   * This is set to true during Phase 1 when user responses are gathered and set to false later when
   * the public input period has passed.
   */
  @Prop() readonly allowInput: boolean = true;

  /**
   * The bounding box for the map, currently used to bound the address search area
   */
  @Prop() readonly bbox: string;

  /**
   * The url to the Features API endpoint to add comments
   */
  @Prop() readonly commentUrl: string;

  /**
   * The endpoint of the geocode api this component connects to.
   */
  @Prop() readonly forwardGeocodeUrl: string;

  /**
   * The Features API url for managing comment likes.
   */
  @Prop() readonly likeUrl: string;

  /**
   * Whether or not this map is expected to be used by multiple people accessing it from the same device.
   * If set to true the intro will play on each page load and users will be prompted to take the survey multiple times.
   */
  @Prop() readonly multiuser: boolean = false;

  /**
   * The url to the schema used for the gl-form component.
   */
  @Prop() readonly schemaUrl: string;

  /**
   * The url to the mapboxgl style used to display the map.
   */
  @Prop() readonly styleUrl: string;

  /**
   * Url to the survey (currently google form) the users will be filling out about
   * the Lincoln Avenue Corridor Study.
   */
  @Prop() readonly surveyUrl: string;

  /**
   * The authentication token for the Features API to access comments.
   */
  @Prop() readonly token: string;

  @State() selectedComment: Feature<Point>;

  private closeDrawer() {
    let drawer = this.el.querySelector("gl-drawer");
    if (drawer != undefined && drawer.open) drawer.toggle();
  }

  private openDrawer() {
    let drawer = this.el.querySelector("gl-drawer");
    if (drawer != undefined && !drawer.open) drawer.toggle();
  }

  @Listen("body:glFeatureClick")
  handleClick(e: CustomEvent) {
    const features = e.detail.features;
    if (!features || !features.length) return;
    const feature = features[0];
    if (feature.layer.id === "lrtp:comment") {
      this.handleCommentClick();
    } else {
      const zoom = this.map.map.getZoom();
      const maxZoom = this.map.map.getMaxZoom();
      this.map.map.easeTo({
        center: feature.geometry.coordinates,
        zoom: zoom + 1 <= maxZoom ? zoom + 1 : maxZoom,
        duration: 500,
      });
    }
  }

  @Listen("commentSelectedEvent")
  selectComment(e: CustomEvent<Feature<Point>>) {
    this.selectedComment = e.detail;
    this.openDrawer();
  }

  private async openSurvey() {
    let alert = await this.surveyCtrl.create();
    return await alert.present();
  }

  private async handleCommentClick() {
    this.closeDrawer();
  }

  private showIntro() {
    let driver: Driver = createDriver({
      animate: false,
      nextBtnText: _t("lrtp.app.intro.next"),
      prevBtnText: _t("lrtp.app.intro.prev"),
      doneBtnText: _t("lrtp.app.intro.done"),
    });

    let steps = [
      {
        element: "gl-map",
        popover: {
          title: _t("lrtp.app.intro.welcome.title"),
          description: `<p>${_t("lrtp.app.intro.welcome.text", {
            app_label: "<strong>" + _t("lrtp.app.label") + "</strong>",
          })}</p><p>(1 / ${this.allowInput ? 6 : 3})</p>`,
        },
      },
      {
        element: "lrtp-comment-detail",
        popover: {
          title: _t("lrtp.app.intro.view.title"),
          description: `<p>${_t("lrtp.app.intro.view.text")}</p><p>(2 / ${
            this.allowInput ? 6 : 3
          })</p>`,
        },
      },
      {
        element: ".lrtp-locate-button",
        popover: {
          title: _t("lrtp.app.intro.locate.title"),
          description: `<p>${_t("lrtp.app.intro.locate.text")}</p><p>(3 / ${
            this.allowInput ? 6 : 3
          })</p>`,
          position: "left",
        },
      },
    ];

    if (this.allowInput)
      steps.push(
        {
          element: "gl-like-button",
          popover: {
            title: _t("lrtp.app.intro.like.title"),
            description: `<p>${_t(
              "lrtp.app.intro.like.text"
            )}</p><p>(4 / 6)</p>`,
            position: "left",
          },
        },
        {
          element: "ion-fab",
          popover: {
            title: _t("lrtp.app.intro.add.title"),
            description: `<p>${_t(
              "lrtp.app.intro.add.text"
            )}</p><p>(5 / 6)</p>`,
            position: "left",
          },
        },
        {
          element: ".lrtp-survey-button",
          popover: {
            title: _t("lrtp.app.intro.survey.title"),
            description: `<p>${_t(
              "lrtp.app.intro.survey.text"
            )}</p><p>(6 / 6)</p>`,
            position: "left",
          },
        }
      );

    driver.setSteps(steps);
    driver.drive();
  }

  async componentWillLoad() {
    if (this.multiuser) localStorage.clear();
  }

  async componentDidLoad() {
    if (this.map.map != undefined) {
      // Here we limit the map to just the lincoln avenue corridor study area, as defined by the extant of G:\CUUATS\Lincoln Avenue Corridor Study\Data\Area_of_Influence.gpkg
      this.map.map.setMaxBounds([
        [-88.2239997, 40.0945542],
        [-88.2142006, 40.112803],
      ]);

      // disable map rotation using right click + drag
      this.map.map.dragRotate.disable();

      // disable map rotation using touch rotation gesture
      this.map.map.touchZoomRotate.disableRotation();
    }

    if (this.surveyCtrl == undefined)
      this.surveyCtrl = await findOrCreateOnReady("lrtp-survey-controller");

    let shown = localStorage.getItem("lrtp.intro") === "true";
    if (!shown) {
      localStorage.setItem("lrtp.intro", "true");
      // TODO: Figure out a better way to determine when the feature list
      // is loaded.
      setTimeout(() => this.showIntro(), 1000);
    }
  }

  private clickedDrawerLabel = () => {
    if (this.drawerToggle != undefined)
      (
        this.drawerToggle.querySelector(
          'ion-button, [title="Toggle drawer"]'
        ) as HTMLElement
      ).click();
  };

  private doClickSurvey = () => {
    if (this.surveyButton != undefined) this.surveyButton.click();
  };

  private doOpenSurvey = () => this.openSurvey();

  private doCloseDrawer = () => this.closeDrawer();

  render() {
    const surveyButton = this.allowInput
      ? [
          <ion-label slot="end-buttons" onClick={this.doClickSurvey}>
            {_t("lrtp.button-labels.survey")}
          </ion-label>,
          <ion-button
            slot="end-buttons"
            class="lrtp-survey-button"
            onClick={this.doOpenSurvey}
            ref={(elm: HTMLIonButtonElement) => {
              this.surveyButton = elm;
            }}
          >
            <ion-icon slot="icon-only" name="bulb"></ion-icon>
          </ion-button>,
        ]
      : null;

    const addButton = this.allowInput ? (
      <ion-fab vertical="bottom" horizontal="end">
        <lrtp-comment-add
          url={this.commentUrl}
          token={this.token}
          onClick={this.doCloseDrawer}
          schema={this.schemaUrl}
          label={_t("lrtp.app.comment.add")}
          toolbarLabel={_t("lrtp.app.comment.location")}
        ></lrtp-comment-add>
      </ion-fab>
    ) : null;

    return (
      <Host>
        <gl-app label={_t("lrtp.app.label")} menu={false}>
          <gl-fullscreen slot="start-buttons"></gl-fullscreen>
          <gl-basemaps slot="start-buttons"></gl-basemaps>
          {surveyButton}
          <ion-label slot="end-buttons" onClick={this.clickedDrawerLabel}>
            {_t("lrtp.button-labels.drawer")}
          </ion-label>
          <gl-drawer-toggle
            slot="end-buttons"
            icon="chatbubbles"
            ref={(elm: HTMLGlDrawerToggleElement) => {
              this.drawerToggle = elm;
            }}
          ></gl-drawer-toggle>
          <gl-address-search
            bbox={this.bbox}
            url={this.forwardGeocodeUrl}
          ></gl-address-search>
          <gl-map
            ref={(r: HTMLGlMapElement) => (this.map = r)}
            longitude={-88.219373}
            latitude={40.105834}
            zoom={13}
            maxzoom={22}
          >
            <gl-style
              url={this.styleUrl}
              id="lrtp"
              clickableLayers={["cluster", "comment"]}
              name={_t("lrtp.app.comment.label")}
              enabled={true}
              token={this.token}
            >
              <gl-popup
                layers={["comment"]}
                component="lrtp-comment-detail"
                componentOptions={{
                  popup: true,
                }}
              ></gl-popup>
            </gl-style>
            <gl-style
              url="https://maps.ccrpc.org/basemaps/imagery-2020/style.json"
              basemap={true}
              thumbnail="https://maps.ccrpc.org/tiles/imagery/13/2087/3098.png"
              name={_t("lrtp.app.basemap.imagery")}
              enabled={false}
            ></gl-style>
            <gl-style
              url="https://maps.ccrpc.org/basemaps/hybrid-2020/style.json"
              basemap={true}
              thumbnail="https://maps.ccrpc.org/tiles/imagery/13/2087/3098.png"
              name={_t("lrtp.app.basemap.hybrid")}
              enabled={true}
            ></gl-style>
          </gl-map>
          {addButton}
          <gl-drawer
            slot="after-content"
            open={true}
            drawer-title={_t("lrtp.app.comment.label")}
          >
            <gl-feature-list
              styleId="lrtp"
              sourceId="comments"
              item={false}
              component="lrtp-comment-detail"
              componentOptions={{
                allowLike: this.allowInput,
                likeUrl: this.likeUrl,
                token: this.token,
                selectedId:
                  this.selectedComment != undefined
                    ? this.selectedComment.id
                    : null,
              }}
              orderBy="_created"
              order="desc"
            ></gl-feature-list>
          </gl-drawer>
          <gl-draw-toolbar slot="footer"></gl-draw-toolbar>
        </gl-app>
      </Host>
    );
  }
}
