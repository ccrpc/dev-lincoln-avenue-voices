# lrtp-app



<!-- Auto Generated Below -->


## Properties

| Property            | Attribute             | Description                                                                                                                                                                                                             | Type      | Default     |
| ------------------- | --------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------- | ----------- |
| `allowInput`        | `allow-input`         | Whether users who view the map are allowed to add new comments or use the survey. This is set to true during Phase 1 when user responses are gathered and set to false later when the public input period has passed.   | `boolean` | `true`      |
| `bbox`              | `bbox`                | The bounding box for the map, currently used to bound the address search area                                                                                                                                           | `string`  | `undefined` |
| `commentUrl`        | `comment-url`         | The url to the Features API endpoint to add comments                                                                                                                                                                    | `string`  | `undefined` |
| `forwardGeocodeUrl` | `forward-geocode-url` | The endpoint of the geocode api this component connects to.                                                                                                                                                             | `string`  | `undefined` |
| `likeUrl`           | `like-url`            | The Features API url for managing comment likes.                                                                                                                                                                        | `string`  | `undefined` |
| `multiuser`         | `multiuser`           | Whether or not this map is expected to be used by multiple people accessing it from the same device. If set to true the intro will play on each page load and users will be prompted to take the survey multiple times. | `boolean` | `false`     |
| `schemaUrl`         | `schema-url`          | The url to the schema used for the gl-form component.                                                                                                                                                                   | `string`  | `undefined` |
| `styleUrl`          | `style-url`           | The url to the mapboxgl style used to display the map.                                                                                                                                                                  | `string`  | `undefined` |
| `surveyUrl`         | `survey-url`          | Url to the survey (currently google form) the users will be filling out about the Lincoln Avenue Corridor Study.                                                                                                        | `string`  | `undefined` |
| `token`             | `token`               | The authentication token for the Features API to access comments.                                                                                                                                                       | `string`  | `undefined` |


## Dependencies

### Depends on

- ion-label
- ion-button
- ion-icon
- ion-fab
- [lrtp-comment-add](../comment-add)
- gl-app
- gl-fullscreen
- gl-basemaps
- gl-drawer-toggle
- gl-address-search
- gl-map
- gl-style
- gl-popup
- gl-drawer
- gl-feature-list
- gl-draw-toolbar

### Graph
```mermaid
graph TD;
  lrtp-app --> ion-label
  lrtp-app --> ion-button
  lrtp-app --> ion-icon
  lrtp-app --> ion-fab
  lrtp-app --> lrtp-comment-add
  lrtp-app --> gl-app
  lrtp-app --> gl-fullscreen
  lrtp-app --> gl-basemaps
  lrtp-app --> gl-drawer-toggle
  lrtp-app --> gl-address-search
  lrtp-app --> gl-map
  lrtp-app --> gl-style
  lrtp-app --> gl-popup
  lrtp-app --> gl-drawer
  lrtp-app --> gl-feature-list
  lrtp-app --> gl-draw-toolbar
  ion-button --> ion-ripple-effect
  lrtp-comment-add --> ion-fab-button
  lrtp-comment-add --> ion-icon
  ion-fab-button --> ion-icon
  ion-fab-button --> ion-ripple-effect
  gl-app --> ion-menu
  gl-app --> ion-header
  gl-app --> ion-toolbar
  gl-app --> ion-title
  gl-app --> ion-content
  gl-app --> ion-menu-toggle
  gl-app --> ion-button
  gl-app --> ion-icon
  gl-app --> ion-buttons
  gl-app --> ion-footer
  gl-app --> ion-split-pane
  gl-app --> ion-app
  ion-menu --> ion-backdrop
  gl-fullscreen --> ion-button
  gl-fullscreen --> ion-icon
  gl-basemaps --> gl-basemap-switcher
  gl-basemaps --> ion-button
  gl-basemaps --> ion-icon
  gl-basemap-switcher --> ion-item
  gl-basemap-switcher --> ion-thumbnail
  gl-basemap-switcher --> ion-radio
  gl-basemap-switcher --> ion-label
  gl-basemap-switcher --> ion-content
  gl-basemap-switcher --> ion-list
  gl-basemap-switcher --> ion-radio-group
  gl-basemap-switcher --> ion-list-header
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  gl-drawer-toggle --> ion-button
  gl-drawer-toggle --> ion-icon
  gl-address-search --> ion-item
  gl-address-search --> ion-icon
  gl-address-search --> ion-label
  gl-address-search --> ion-list
  gl-address-search --> ion-searchbar
  ion-searchbar --> ion-icon
  gl-drawer --> ion-header
  gl-drawer --> ion-toolbar
  gl-drawer --> ion-buttons
  gl-drawer --> ion-title
  gl-drawer --> ion-button
  gl-drawer --> ion-icon
  gl-drawer --> ion-content
  gl-feature-list --> ion-item
  gl-feature-list --> ion-list
  gl-feature-list --> ion-infinite-scroll
  gl-feature-list --> ion-infinite-scroll-content
  ion-infinite-scroll-content --> ion-spinner
  gl-draw-toolbar --> ion-toolbar
  gl-draw-toolbar --> ion-title
  gl-draw-toolbar --> ion-buttons
  gl-draw-toolbar --> ion-button
  gl-draw-toolbar --> ion-icon
  style lrtp-app fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
