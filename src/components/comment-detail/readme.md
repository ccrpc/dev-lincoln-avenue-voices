# lrtp-comment-detail



<!-- Auto Generated Below -->


## Properties

| Property     | Attribute     | Description                                                                                                                                                                                                    | Type                                         | Default     |
| ------------ | ------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------- | ----------- |
| `allowLike`  | `allow-like`  | Whether the user is allowed to like (or un-like) comments                                                                                                                                                      | `boolean`                                    | `true`      |
| `feature`    | --            | The GeoJSON feature relating to this comment.                                                                                                                                                                  | `Feature<Point, { [name: string]: any; }>`   | `undefined` |
| `features`   | --            | Some things (such as gl-popup) will pass along a list of features with one item it in rather than setting the "feature" property. This is here to handle that scenario and extract the feature from it.        | `Feature<Point, { [name: string]: any; }>[]` | `undefined` |
| `likeUrl`    | `like-url`    | The Features API url for liking and un-liking comments                                                                                                                                                         | `string`                                     | `undefined` |
| `popup`      | `popup`       | Whether or not to display the like and locate buttons. TODO This is currently called 'popup' as it references whether this comment is in a popup or not. Consider renaming to something like 'include buttons' | `boolean`                                    | `false`     |
| `selectedId` | `selected-id` | The id of the currently selected comment (for the purposes of highlighting in the gl-feature-list)                                                                                                             | `number`                                     | `undefined` |
| `token`      | `token`       | The authentication token to communicate with the Features API                                                                                                                                                  | `string`                                     | `undefined` |


## Events

| Event                  | Description                                                       | Type                                                    |
| ---------------------- | ----------------------------------------------------------------- | ------------------------------------------------------- |
| `commentSelectedEvent` | Emitted when the use chooses a comment as the 'selected' comment. | `CustomEvent<Feature<Point, { [name: string]: any; }>>` |


## Dependencies

### Depends on

- ion-avatar
- ion-label
- ion-button
- ion-icon
- gl-like-button
- ion-item

### Graph
```mermaid
graph TD;
  lrtp-comment-detail --> ion-avatar
  lrtp-comment-detail --> ion-label
  lrtp-comment-detail --> ion-button
  lrtp-comment-detail --> ion-icon
  lrtp-comment-detail --> gl-like-button
  lrtp-comment-detail --> ion-item
  ion-button --> ion-ripple-effect
  gl-like-button --> ion-button
  gl-like-button --> ion-icon
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  style lrtp-comment-detail fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
