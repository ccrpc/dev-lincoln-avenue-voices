import { HTMLStencilElement } from "@stencil/core/internal";

export async function findOrCreateOnReady<
  E extends HTMLStencilElement = HTMLStencilElement
>(tagName: string): Promise<E> {
  let elm = document.querySelector<E>(tagName);
  if (elm == null) {
    elm = document.createElement(tagName) as E;
    document.body.appendChild(elm);
  }
  return typeof elm.componentOnReady === "function"
    ? elm.componentOnReady()
    : Promise.resolve(elm);
}
