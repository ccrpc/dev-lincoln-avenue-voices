export const config = {
  namespace: "lrtp",
  outputTargets: [
    {
      type: "docs-readme",
    },
    {
      type: "www",
      baseUrl: "/lincoln-avenue-voices",
      serviceWorker: null,
      copy: [
        {
          src: "public",
        },
      ],
    },
  ],
  plugins: [],
  globalScript: "src/global/lrtp.ts",
  globalStyle: "src/global/lrtp.css",
};
